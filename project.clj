(defproject e1 "0.1.0-SNAPSHOT"
  :description "Generate some HTML/CSS/JS"
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [hiccup "1.0.5"]
                 [garden "1.2.5"]
                 [org.clojure/clojurescript "0.0-3126"]
                 [org.clojure/tools.nrepl "0.2.9"]]
  
  :plugins [[cider/cider-nrepl "0.9.0-SNAPSHOT"]
            [lein-cljsbuild "1.0.5"]]

  :cljsbuild {
              :builds {
                       :main {
                              :source-paths ["src-cljs"]
                              :compiler {
                                         :output-to "resources/main.js"}
                              }
                       }
              }
  
  :main e1.core
  :aot [e1.core]
  
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
