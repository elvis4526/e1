(ns e1.core
  (:gen-class)
  (:use hiccup.core)
  (:use garden.core)
  )


(defn generate-css
  "Generate some CSS"
  []
  (css [:h1 {:color "red" :font-size "14px"}])
  )

(defn generate-html
  "Generate some HTML"
  []
  [:h1
   "This is some generated HTML"]
  )

(defn generate-page
  "Generate a /complete/ webpage"
  []
  (html [:html
         [:head
          [:style (generate-css)]]
         [:body (generate-html)]])
  )

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println "Generated HTML:")
  (println (generate-page))
  )
           
